package com.example.chatroomapplication.Enums;

import java.io.Serializable;

public enum Status implements Serializable {
    JOIN, MESSAGE, LEAVE;
}
