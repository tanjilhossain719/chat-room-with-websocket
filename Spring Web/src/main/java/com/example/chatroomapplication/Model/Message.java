package com.example.chatroomapplication.Model;

import com.example.chatroomapplication.Enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "chatRoomMessages")
public class Message implements Serializable {

    private static final long serialVersionUID = 58987546524597L;

    @Id
    private String id;

    @NotNull(message = "Sender Id must not be null")
    private String senderId;

//    @NotNull(message = "Receiver Id must not be null")
//    private String receiverId;

    @NotNull(message = "Please you can't send null message")
    private String message;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonIgnore
    private boolean deleteMessage;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private long createdAt;

    private Status status;


}
