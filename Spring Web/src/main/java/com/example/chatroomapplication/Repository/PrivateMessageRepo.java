package com.example.chatroomapplication.Repository;

import com.example.chatroomapplication.Model.PrivateMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivateMessageRepo extends MongoRepository<PrivateMessage, String> {

}
