package com.example.chatroomapplication.Repository;

import com.example.chatroomapplication.Model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepo extends MongoRepository<Message, String > {

}
