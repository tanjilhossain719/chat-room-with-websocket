package com.example.chatroomapplication.Controller;


import com.example.chatroomapplication.Model.Message;
import com.example.chatroomapplication.Model.PrivateMessage;
import com.example.chatroomapplication.Repository.MessageRepo;
import com.example.chatroomapplication.Repository.PrivateMessageRepo;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/messaging")
public class ChatController {

    private MessageRepo messageRepo;
    private PrivateMessageRepo privateMessageRepo;
    private SimpMessagingTemplate simpMessagingTemplate;


    //***********************************************************************//
    //*************This method is for a chatroom or group chat***************//
    //***********************************************************************//

    //Client will send message to this URL
    @MessageMapping("/message")
    //Client who subscribed this link will get this message
    @SendTo("/chatroom/return-to-public")
    public Message connectionAndSaveMessage(@RequestBody Message message) {
        try {
            message.setCreatedAt(LocalDateTime.now().getLong(ChronoField.EPOCH_DAY));
            messageRepo.save(message);
            //it will wait for .5 second. After that it will return message
            Thread.sleep(500);

        } catch (InterruptedException e) {
            //it will print the line number and class name where the exception occurred
            e.printStackTrace();
        }
        return message;
    }



    //***********************************************************************//
    //*****************This method is for private chat***********************//
    //***********************************************************************//
    @MessageMapping("/private")
    public PrivateMessage connectionPrivateAndSaveMessage(@RequestBody PrivateMessage message) {
        try {

            message.setCreatedAt(LocalDateTime.now().getLong(ChronoField.EPOCH_DAY));
            privateMessageRepo.save(message);
            //convertAndSendToUser will automatically connect to the setUserDestinationPrefix.
            //It will work as dynamic. 1st user ID then client side sender URL and the message want to send
            simpMessagingTemplate.convertAndSendToUser(message.getReceiverId(), "/private", message);
            Thread.sleep(500);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return message;
    }


}
