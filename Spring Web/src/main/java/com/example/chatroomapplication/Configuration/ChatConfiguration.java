package com.example.chatroomapplication.Configuration;


import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class ChatConfiguration implements WebSocketMessageBrokerConfigurer {

    //This is creating server connection URL
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //Client will connect with server when they try to send message,
        //By this URL
        registry.addEndpoint("/chat").setAllowedOriginPatterns("*").withSockJS();
    }

    // Send and receive message connection with client
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {

        //Client will send the message with this URL first /app then the URL set in @MessageMapping
        //like /app/temp {}. {}-> will contain the message
        registry.setApplicationDestinationPrefixes("/app");

        //the message will broadcast to this URL. Person who subscribed the link @ with @SendTo will get the message
        registry.enableSimpleBroker("/chatroom", "/user");

        // For specific user/client it set the URL. The user will get private message
        registry.setUserDestinationPrefix("/user");
    }
}
