import React, { useEffect, useState } from 'react'
import {over} from 'stompjs';
import SockJS from 'sockjs-client';

var stompClient =null;
const ChatRoom = () => {
    
    const [privateChats, setPrivateChats] = useState(new Map());     
    const [publicChats, setPublicChats] = useState([]); 
    const [tab,setTab] =useState("CHATROOM");
    const [userData, setUserData] = useState({
        userId: '',
        receiverId: '',
        connected: false,
        message: ''
      });
    useEffect(() => {
      console.log(userData);
    }, [userData]);

    const connect =()=>{
        let Sock = new SockJS('http://localhost:8080/chat');
        stompClient = over(Sock);
        stompClient.connect({},onConnected, onError);
    }

    console.log('stomp client', stompClient)

    const onConnected = () => {
        setUserData({...userData,"connected": true});
        stompClient.subscribe('/chatroom/return-to-public', onMessageReceived);
        stompClient.subscribe('/user/'+userData.userId+'/private', onPrivateMessage);
        userJoin();
    }

    const userJoin=()=>{
          var chatMessage = {
            senderId: userData.userId,
            status:"JOIN"
          };
          stompClient.send("/app/message", {}, JSON.stringify(chatMessage));
    }

    const onMessageReceived = (payload)=>{
        var payloadData = JSON.parse(payload.body);
        switch(payloadData.status){
            case "JOIN":
                if(!privateChats.get(payloadData.senderId)){
                    privateChats.set(payloadData.senderId,[]);
                    setPrivateChats(new Map(privateChats));
                }
                break;
            case "MESSAGE":
                publicChats.push(payloadData);
                setPublicChats([...publicChats]);
                break;
        }
    }
    
    const onPrivateMessage = (payload)=>{
        console.log(payload);
        var payloadData = JSON.parse(payload.body);
        console.log(payloadData)
        if(privateChats.get(payloadData.senderId)){
            privateChats.get(payloadData.senderId).push(payloadData);
            setPrivateChats(new Map(privateChats));
        }else{
            let list =[];
            list.push(payloadData);
            privateChats.set(payloadData.senderId,list);
            setPrivateChats(new Map(privateChats));
        }
    }

    const onError = (err) => {
        console.log(err);
        
    }

    const handleUserName=(event)=>{
        const {value, name}=event.target;
        setUserData({...userData,[name]: value});
    }

     
    const handleMessage =(event)=>{
        const {value}=event.target;
        setUserData({...userData,"message": value});
    }
    const sendValue=()=>{
        
            if (stompClient) {
                console.log('clicked')
              var chatMessage = {
                senderId: userData.userId,
                message: userData.message,
                status:"MESSAGE"
              };
              console.log(chatMessage);
              stompClient.send("/app/message", {}, JSON.stringify(chatMessage));
              setUserData({...userData,"message": ""});
            }
    }

    const sendPrivateValue=()=>{
        if (stompClient) {
          var chatMessage = {
            senderId: userData.userId,
            receiverId:tab,
            message: userData.message,
            status:"MESSAGE"
          };
          
          if(userData.userId !== tab){
            privateChats.get(tab).push(chatMessage);
            setPrivateChats(new Map(privateChats));
          }
          stompClient.send("/app/private", {}, JSON.stringify(chatMessage));
          setUserData({...userData,"message": ""});
        }
    }

    const registerUser=()=>{
        connect();
    }
    return (
    <div className="container">
        {userData.connected?
        <div className="chat-box">
            <div className="member-list">
                <ul>
                    <li onClick={()=>{setTab("CHATROOM")}} className={`member ${tab==="CHATROOM" && "active"}`}>Chatroom</li>
                    {[...privateChats.keys()].map((name,index)=>(
                        <li onClick={()=>{setTab(name)}} className={`member ${tab===name && "active"}`} key={index}>{name}</li>
                    ))}
                </ul>
            </div>
            {tab==="CHATROOM" && <div className="chat-content">
                <ul className="chat-messages">
                    {publicChats.map((chat,index)=>(
                        <li className={`message ${chat.senderId === userData.userId && "self"}`} key={index}>
                            {chat.senderId !== userData.userId && <div className="avatar">{chat.senderId}</div>}
                            <div className="message-data">{chat.message}</div>
                            {chat.senderId === userData.userId && <div className="avatar self">{chat.senderId}</div>}
                        </li>
                    ))}
                </ul>

                <div className="send-message">
                    <input type="text" className="input-message"  name ='message' placeholder="Enter public message" value={userData.message} onChange={handleUserName} /> 
                    <button type="button" className="send-button" onClick={sendValue}>send</button>
                </div>
            </div>}
            {tab!=="CHATROOM" && <div className="chat-content">
                <ul className="chat-messages">
                    {[...privateChats.get(tab)].map((chat,index)=>(
                        <li className={`message ${chat.senderId === userData.userId && "self"}`} key={index}>
                            {chat.senderId !== userData.userId && <div className="avatar">{chat.senderId}</div>}
                            <div className="message-data">{chat.message}</div>
                            {chat.senderId === userData.userId && <div className="avatar self">{chat.senderId}</div>}
                        </li>
                    ))}
                </ul>  

                <div className="send-message">
                    <input type="text" className="input-message" name ='message' placeholder={`Enter private message for ${tab}`} value={userData.message} onChange={handleUserName} /> 
                    <button type="button" className="send-button" onClick={sendPrivateValue}>send</button>
                </div>
            </div>}
        </div>
        :
        <div className="register">
            <input
                id="user-id"
                placeholder="Enter your Id"
                name="userId"
                value={userData.userId}
                onChange={handleUserName}
                margin="normal"
              />
              <button type="button" onClick={registerUser}>
                    connect
              </button> 
        </div>}
    </div>
    )
}

export default ChatRoom
